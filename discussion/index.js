// Function Parameters and Arguments


// "name" is called a parameter that acts a named variable or container that exist only inside of the function

// "Nidz" - argument - it is the information or data provided directly into the function.

function printInput(name){
	console.log("Hi my name is " + name);
};


printInput("Nidz");

printInput("Legend");

let sampleVariable = "Yui";

printInput(sampleVariable);






function checkDivisibilityBy8(num){
				let remainder = num % 8;
				console.log("The remainder of " + num + " divided by 8 is: " + remainder);
				let isDivisibleBy8 = remainder === 0;
				console.log("Is " + num + " divisible by 8?");
				console.log(isDivisibleBy8);
			}

			checkDivisibilityBy8(64);
			checkDivisibilityBy8(28);




// Function as Arguments ******


// Function paramenters can also accept other functions as arguments


function argumentFunction(){
	/*console.log("This function was passed as an argument before the message was printed.")*/

	// add
	console.log(5 + 15);
};


function invokeFunction(argumentFunction){
	argumentFunction();
	/*console.log("This code comes from invokeFunction");*/

	console.log(3 - 4);
};


invokeFunction(argumentFunction);

// Multiple Arguments
 	// -will correspond to the number of "Parameters" declared in a function in succeeding order. 


/* function createFullName(firstName,middleName,lastName){
 	console.log(firstName + ' ' + middleName + ' ' + lastName);
 };


 createFullName("Nidzhar","D.");
 createFullName("Nidzhars","Ds.","","sure");


 // ======================================

let firstName = "John";
let middleName = "Dela";
let lastName = "Pena";

createFullName(firstName, middleName, lastName);*/



// ****************************************

// The Return Statements


// The return statement allows us to output a value from a function to be passed to the line/block of code that invoked/called the function.

// The return statement also stops the execution of the function and any code after the return statement will not be executed.

function returnFullname(firstName, middleName, lastName){
	console.log("This message will not be printed");
	return firstName + ' ' + middleName + ' ' + lastName;

	console.log("This message will not be printed");
};


let completeName = returnFullname("Jawo","Jimenez","Gabo");

console.log(completeName);


function returnAddress(city, country){
	let fullAddress = city + ", " + country;
	return fullAddress;
}


let myAddress = returnAddress("Zamboanga","PH");
console.log(myAddress);